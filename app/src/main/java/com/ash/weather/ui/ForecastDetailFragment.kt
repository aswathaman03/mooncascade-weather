package com.ash.weather.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import com.ash.weather.R
import com.ash.weather.repository.data.Phenomenon
import com.ash.weather.repository.data.Place
import com.ash.weather.utils.NumberConverter
import kotlinx.android.synthetic.main.fragment_weather_detail.*

private const val PLACE = "Place"

class ForecastDetailFragment : Fragment() {
    private var place: Place? = null
    private var phenomenonDrawable: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            place = it.getParcelable<Place>(PLACE)
            phenomenonDrawable = Phenomenon.valueOf(place!!.phenomenon.replace(" ", "_")).icon
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_weather_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateUi()
    }

    fun populateUi() {
        with(place!!) {
            placeName.text = name
            tempMax.text = getString(R.string.temp_celsius_in_words, NumberConverter.convertNumberToWords(tempmax))
            tempMin.text = getString(R.string.temp_celsius_in_words, NumberConverter.convertNumberToWords(tempmin))
            phenomenonIcon.setImageDrawable(ContextCompat.getDrawable(phenomenonIcon.context, phenomenonDrawable!!))
            forecast.text = phenomenon
        }
    }

}
