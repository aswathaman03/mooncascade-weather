package com.ash.weather.ui

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ash.weather.R
import com.ash.weather.adapter.ForecastListAdapter
import com.ash.weather.repository.data.Forecast
import com.ash.weather.viewModel.ForecastListViewModel
import com.ash.weather.viewModel.ForecastListViewModelFactory
import kotlinx.android.synthetic.main.fragment_forecast.*

class ForecastFragment : Fragment() {

    private val forecastListViewModel: ForecastListViewModel by viewModels {
        ForecastListViewModelFactory(requireContext())
    }


    var forecastListAdapter: ForecastListAdapter? = null
    var rvLayoutManager: RecyclerView.LayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_forecast, container, false).also { subscribe() }
    }

    private fun subscribe() {
        forecastListViewModel.loading.observe(
            this,
            Observer { value ->
                if (value) {
                    loadingView.visibility = View.VISIBLE
                    forecastRecyclerView.visibility = View.GONE
                } else {
                    loadingView.visibility = View.GONE
                    forecastRecyclerView.visibility = View.VISIBLE
                }
            })

        forecastListViewModel.loadError.observe(
            this,
            Observer { value ->
                if (value) {
                    loadingView.visibility = View.GONE
                    forecastRecyclerView.visibility = View.GONE
                    errorText.visibility = View.VISIBLE
                    errorText.text =
                        if (!checkForInternetConnection()) "No internet connection" else "Server connection failed"
                }else{
                    errorText.visibility = View.GONE
                }
            })

        forecastListViewModel.forecasts.observe(this, Observer { value ->
            forecastRecyclerView.visibility = View.VISIBLE
            setRecyclerAdapter(value)
        })
    }

    fun setRecyclerAdapter(forecast: ArrayList<Forecast>) {
        forecastRecyclerView.visibility = View.VISIBLE
        forecastRecyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext()).also {
                if (rvLayoutManager != null)
                    it.onRestoreInstanceState(rvLayoutManager!!.onSaveInstanceState())
                rvLayoutManager = it
            }
            adapter = forecastListAdapter ?: ForecastListAdapter(forecast).also { forecastListAdapter = it }
        }
    }


    override fun onResume() {
        super.onResume()
        if (forecastListViewModel.loadError.value!! && checkForInternetConnection())
            forecastListViewModel.fetchForecast()
    }

    fun checkForInternetConnection(): Boolean {
        val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnected == true
        return isConnected
    }
}