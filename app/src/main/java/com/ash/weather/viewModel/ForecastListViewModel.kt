package com.ash.weather.viewModel

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ash.weather.di.DaggerRepositoryModuleInjector
import com.ash.weather.di.DatabaseModule
import com.ash.weather.di.NetworkModule
import com.ash.weather.repository.data.Forecast
import com.ash.weather.repository.data.ForecastRepository
import com.ash.weather.repository.data.Forecasts
import com.ash.weather.repository.network.RepoService
import com.ash.weather.utils.GeneralUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.collections.ArrayList

class ForecastListViewModel(context: Context) : ViewModel() {

    @Inject
    lateinit var repoService: RepoService
    @Inject
    lateinit var forecastRepository: ForecastRepository

    private lateinit var subscription: Disposable

    private val current_date = GeneralUtils.getDate()

    var forecasts = MutableLiveData<ArrayList<Forecast>>()
    var loadError = MutableLiveData<Boolean>()
    var loading = MutableLiveData<Boolean>()

    init {
        DaggerRepositoryModuleInjector.builder()
            .databaseModule(DatabaseModule(context))
            .networkModule(NetworkModule)
            .build().inject(this)
        fetchForecast()
    }

    fun fetchForecast() {
        subscription =
            Observable.fromCallable { forecastRepository.getCache(current_date) }
                .concatMap { dbForecasts ->
                    if (dbForecasts.isNullOrEmpty()) {
                        repoService.getForecasts().concatMap {
                            viewModelScope.launch(Dispatchers.IO) {
                                forecastRepository.insert(it.apply { date = current_date })
                            }
                            Observable.just(it)
                        }
                    } else {
                        Observable.just(dbForecasts[0])
                    }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onSubscribed() }
                .doOnTerminate { onTerminated() }
                .subscribe({ value ->
                    onCompleted(value.forecasts)
                }, {
                    onError()
                })
    }


    private fun onSubscribed() {
        loading.value = true
        loadError.value = false
    }

    private fun onTerminated() {
        loading.value = false
    }

    private fun onError() {
        loading.value = false
        loadError.value = true
    }


    private fun onCompleted(forecastList: ArrayList<Forecast>) {
        if (!forecastList.isNullOrEmpty())
            forecasts.value = forecastList
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}