package com.ash.weather.di

import android.content.Context
import com.ash.weather.repository.data.ForecastRepository
import com.ash.weather.repository.data.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(private var context: Context) {

    @Singleton
    @Provides
    fun provideContext(): Context {
        return context
    }

    @Singleton
    @Provides
    fun provideForecastRepository(context: Context): ForecastRepository {
        return ForecastRepository(
            AppDatabase.getInstance(
                context
            ).forecastDao()
        )
    }


}