package com.ash.weather.di

import com.ash.weather.viewModel.ForecastListViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [(NetworkModule::class), (DatabaseModule::class)])
interface RepositoryModuleInjector {

    fun inject(forecastListViewModel: ForecastListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): RepositoryModuleInjector

        fun databaseModule(databaseModule: DatabaseModule): Builder

        fun networkModule(networkModule: NetworkModule): Builder
    }
}