package com.ash.weather.repository.data


data class DayOrNight(
    val phenomenon: String,
    val tempmin: Int,
    val tempmax: Int,
    val text: String,
    val sea: String,
    val peipsi: String,
    val places: ArrayList<Place>,
    val winds: ArrayList<Wind>
)
