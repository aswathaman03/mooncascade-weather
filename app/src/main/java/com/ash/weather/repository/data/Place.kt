package com.ash.weather.repository.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Place(val name: String, val phenomenon: String, val tempmin: Int, val tempmax: Int) : Parcelable
