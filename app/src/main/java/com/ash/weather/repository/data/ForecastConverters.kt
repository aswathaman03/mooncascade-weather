package com.ash.weather.repository.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlin.collections.ArrayList

class ForecastConverters {

    @TypeConverter
    fun forecastsToString(forecasts: ArrayList<Forecast>): String {
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Forecast>>() {}.type
        return gson.toJson(forecasts, type)
    }

    @TypeConverter
    fun stringToForecasts(forecastsString: String): ArrayList<Forecast> {
        val forecasts_type = object : TypeToken<ArrayList<Forecast>>() {}.type
        val forecasts: ArrayList<Forecast> = Gson().fromJson(forecastsString, forecasts_type)
        return forecasts
    }


}