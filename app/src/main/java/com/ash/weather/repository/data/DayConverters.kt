package com.ash.weather.repository.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DayConverters {

    @TypeConverter
    fun dayToString(day: DayOrNight): String {
        val gson = Gson()
        val type = object : TypeToken<DayOrNight>() {}.type
        return gson.toJson(day, type)
    }

    @TypeConverter
    fun stringToDay(dayString: String): DayOrNight {
        val dbDay_type = object : TypeToken<DayOrNight>() {}.type
        val dbDay: DayOrNight = Gson().fromJson(dayString, dbDay_type)
        return dbDay
    }


}