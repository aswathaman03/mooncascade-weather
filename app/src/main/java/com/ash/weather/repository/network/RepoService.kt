package com.ash.weather.repository.network

import com.ash.weather.repository.data.Forecasts
import io.reactivex.Observable
import retrofit2.http.GET

interface RepoService {
    @GET("api/estonia/forecast")
    fun getForecasts(): Observable<Forecasts>
}