package com.ash.weather.repository.data


class ForecastRepository  constructor(private val forecastDao: ForecastDao){
    fun getCache(date: String) = forecastDao.getForecasts(date)

    fun insert(forecasts: Forecasts) = forecastDao.insert(forecasts)
}