package com.ash.weather.repository.data



data class Wind(val name: String, val direction: String, val speedmin: Int, val speedmax: Int)