package com.ash.weather.repository.data

import androidx.room.*


data class Forecast(
    val date: String,
    val day: DayOrNight,
    val night: DayOrNight,
    @Ignore var isNight: Boolean = false
)