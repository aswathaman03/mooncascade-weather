package com.ash.weather.repository.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import kotlin.collections.ArrayList

@Entity
data class Forecasts(@PrimaryKey var date: String, @TypeConverters(ForecastConverters::class) val forecasts: ArrayList<Forecast>)