package com.ash.weather.repository.data

import com.ash.weather.R

enum class Phenomenon(val phenomenon : String, val icon : Int) {
    Clear("Clear", R.drawable.ic_clear),
    Few_clouds("Few clouds", R.drawable.ic_few_clouds),
    Variable_clouds("Variable clouds", R.drawable.ic_variable_clouds),
    CLoudy_with_clear_spells("Cloudy with clear spells", R.drawable.ic_cloudy_with_clear_spells),
    Cloudy("Cloudy", R.drawable.ic_cloudy),
    Light_snow_shower("Light snow shower", R.drawable.ic_light_snow_shower),
    Moderate_snow_shower("Moderate snow shower", R.drawable.ic_moderate_snow_shower),
    Heavy_snow_shower("Heavy snow shower", R.drawable.ic_heavy_snow_shower),
    Light_shower("Light shower", R.drawable.ic_light_shower),
    Moderate_shower("Moderate shower", R.drawable.ic_moderate_shower),
    Heavy_shower("Heavy shower", R.drawable.ic_heavy_rain_shower),
    Light_rain("Light rain", R.drawable.ic_light_shower),
    Moderate_rain("Moderate rain", R.drawable.ic_moderate_shower),
    Heavy_rain("Heavy rain", R.drawable.ic_heavy_rain_shower),
    Risk_of_glaze("Risk of glaze", R.drawable.ic_risk_of_glaze),
    Light_sleet("Light sleet", R.drawable.ic_sleet),
    Moderate_sleet("Moderate sleet", R.drawable.ic_sleet),
    Light_snowfall("Light snowfall", R.drawable.ic_light_snow_shower),
    Moderate_snowfall("Moderate snowfall", R.drawable.ic_moderate_snow_shower),
    Heavy_snowfall("Heavy snowfall", R.drawable.ic_heavy_snow_shower),
    Snowstorm("Snowstorm", R.drawable.ic_snow_storm),
    Drifting_snow("Drifting snow", R.drawable.ic_drifting_snow),
    Hail("Hail", R.drawable.ic_hail),
    Mist("Mist", R.drawable.ic_mist),
    Fog("Fog", R.drawable.ic_mist),
    Thunder("Thunder", R.drawable.ic_thunder),
    Thunderstorm("Thunderstorm", R.drawable.ic_thunder_storm)
}