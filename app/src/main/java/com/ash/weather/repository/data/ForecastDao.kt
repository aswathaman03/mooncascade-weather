package com.ash.weather.repository.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ForecastDao {

    @Query("SELECT * FROM forecasts WHERE date = :date")
    fun getForecasts(date: String): List<Forecasts>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(forecasts: Forecasts)

}