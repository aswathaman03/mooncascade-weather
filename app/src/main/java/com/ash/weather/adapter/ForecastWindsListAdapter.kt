package com.ash.weather.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.ash.weather.R
import com.ash.weather.repository.data.Wind
import com.ash.weather.ui.ForecastFragmentDirections
import kotlinx.android.synthetic.main.place_item.view.*

public class ForecastWindsListAdapter(private val winds: ArrayList<Wind>) :
    RecyclerView.Adapter<ForecastWindsListAdapter.ListHolder>() {


    override fun onBindViewHolder(holder: ListHolder, position: Int) {
        holder.bind(winds[position])
    }

    override fun getItemCount(): Int {
        return winds.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.place_item, parent, false)
        return ListHolder(view)
    }


    inner class ListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(wind: Wind) {
            with(wind) {
                itemView.placeName.text = name
                itemView.temp.text = itemView.temp.context.getString(R.string.wind, speedmax, speedmin)
//                itemView.temp.text = "${speedmin} / ${speedmax} m/s"
            }
        }

    }
}