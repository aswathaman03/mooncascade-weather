package com.ash.weather.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.ash.weather.R
import com.ash.weather.repository.data.Place
import com.ash.weather.ui.ForecastFragmentDirections
import kotlinx.android.synthetic.main.place_item.view.*

class ForecastPlacesListAdapter(private val places: ArrayList<Place>) :
    RecyclerView.Adapter<ForecastPlacesListAdapter.ListHolder>() {


    override fun onBindViewHolder(holder: ListHolder, position: Int) {
        holder.bind(places[position])
    }

    override fun getItemCount(): Int {
        return places.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.place_item, parent, false)
        return ListHolder(view)
    }


    inner class ListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.setOnClickListener {
                val direction =
                    ForecastFragmentDirections.actionForecastFragmentToForecastDetailFragment(places[layoutPosition])
                it.findNavController().navigate(direction)
            }
        }

        fun bind(place: Place) {
            with(place) {
                itemView.placeName.text = name
                itemView.temp.text = itemView.temp.context.getString(R.string.place_temp, tempmin, tempmax)
            }
        }

    }
}