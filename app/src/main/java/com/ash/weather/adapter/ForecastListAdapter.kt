package com.ash.weather.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ash.weather.R
import com.ash.weather.repository.data.*
import com.ash.weather.utils.GeneralUtils
import com.ash.weather.utils.changeBackground
import com.ash.weather.utils.changeTextColor
import kotlinx.android.synthetic.main.forecast_item.view.*
import kotlinx.android.synthetic.main.forecast_item.view.phenomenonIcon
import kotlinx.android.synthetic.main.fragment_weather_detail.*


class ForecastListAdapter(private val forecasts: ArrayList<Forecast>) :
    RecyclerView.Adapter<ForecastListAdapter.ListHolder>() {


    override fun onBindViewHolder(holder: ListHolder, position: Int) {
        holder.bind(forecasts[position])
    }

    override fun getItemCount(): Int {
        return forecasts.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.forecast_item, parent, false)
        return ListHolder(view)
    }


    inner class ListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(forecast: Forecast) {
            itemView.dayOrNight.setOnCheckedChangeListener { _, b ->
                animate(b)
                forecast.isNight = b
                if (b) populateViews(forecast.night) else populateViews(forecast.day)
            }
            itemView.date.text = GeneralUtils.getDate(forecast.date)
            with(forecast) {
                populateViews(day)
                itemView.dayOrNight.isChecked = isNight
            }
        }

        private fun populateViews(content: DayOrNight) {
            with(content) {
                itemView.tempMax.text = itemView.context.getString(R.string.temp_celsius, tempmax)
                itemView.tempMin.text = itemView.context.getString(R.string.temp_celsius, tempmin)

                val phenomenonDrawable = Phenomenon.valueOf(content.phenomenon.replace(" ", "_")).icon
                itemView.phenomenonIcon.setImageDrawable(ContextCompat.getDrawable(itemView.phenomenonIcon.context, phenomenonDrawable))
                itemView.phenomenon.text = phenomenon

                if (!text.isNullOrEmpty()) {
                    itemView.text.text = text
                    itemView.tempContainer.visibility = View.VISIBLE
                    itemView.view1.visibility = View.VISIBLE
                } else {
                    itemView.tempContainer.visibility = View.GONE
                    itemView.view1.visibility = View.GONE
                }

                if (sea.isNullOrEmpty() && peipsi.isNullOrEmpty()) {
                    itemView.view1.visibility = View.GONE
                }

                if (!sea.isNullOrEmpty()) {
                    itemView.sea.text = sea
                    itemView.seaContainer.visibility = View.VISIBLE
                    itemView.view2.visibility = View.VISIBLE
                } else {
                    itemView.seaContainer.visibility = View.GONE
                    itemView.view2.visibility = View.GONE
                }

                if (peipsi.isNullOrEmpty()) {
                    itemView.view2.visibility = View.GONE
                }

                if (!peipsi.isNullOrEmpty()) {
                    itemView.wind.text = peipsi
                    itemView.windContainer.visibility = View.VISIBLE
                } else {
                    itemView.windContainer.visibility = View.GONE
                }

                if (!places.isNullOrEmpty())
                    setPlacesRecyclerAdapter(places)
                else {
                    itemView.placesRecyclerView.visibility = View.GONE
                }

                if (!winds.isNullOrEmpty())
                    setWindsRecyclerAdapter(winds)
                else {
                    itemView.windsRecyclerView.visibility = View.GONE
                }
            }
        }

        private fun animate(isNight: Boolean) {
            GeneralUtils.setAnimation(itemView.forecastItemContainer, 300)
            itemView.cardView.changeBackground(isNight)
            itemView.date.changeTextColor(isNight)
            itemView.text.changeTextColor(isNight)
            itemView.tempMin.changeTextColor(isNight)
            itemView.tempMax.changeTextColor(isNight)
            itemView.sea.changeTextColor(isNight)
            itemView.wind.changeTextColor(isNight)
            itemView.view1.changeBackground(isNight)
            itemView.view2.changeBackground(isNight)
        }

        fun setPlacesRecyclerAdapter(places: ArrayList<Place>) {
            itemView.placesRecyclerView.visibility = View.VISIBLE
            itemView.placesRecyclerView.apply {
                layoutManager = GridLayoutManager(this.context, 2)
                adapter = ForecastPlacesListAdapter(places)
            }
        }

        fun setWindsRecyclerAdapter(winds: ArrayList<Wind>) {
            itemView.windsRecyclerView.visibility = View.VISIBLE
            itemView.windsRecyclerView.apply {
                layoutManager = GridLayoutManager(this.context, 2)
                adapter = ForecastWindsListAdapter(winds)
            }
        }
    }

}