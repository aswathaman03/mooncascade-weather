package com.ash.weather.utils


import android.view.ViewGroup
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import com.transitionseverywhere.*
import java.text.SimpleDateFormat
import java.util.*

class GeneralUtils {

    companion object {

        fun setAnimation(view: ViewGroup, time: Long) {
            val set = TransitionSet().apply {
                addTransition(Recolor())
                addTransition(Fade())
                duration = time
                interpolator = FastOutLinearInInterpolator()
            }
            TransitionManager.beginDelayedTransition(view, set)
            TransitionManager.beginDelayedTransition(
                view,
                ChangeText().setChangeBehavior(ChangeText.CHANGE_BEHAVIOR_IN)
            )
        }

        fun getDate(date: String): String {
            val defFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
            val resFormat = SimpleDateFormat("dd-MMM", Locale.US)
            return parseDate(date, defFormat, resFormat)
        }

        fun parseDate(input: String, inputFormat: SimpleDateFormat, outputFormat: SimpleDateFormat): String {
            val date: Date = inputFormat.parse(input)
            val outputString: String = outputFormat.format(date)
            return outputString
        }

        fun getDate(): String {
            val cal = Calendar.getInstance()
            val resFormat = SimpleDateFormat("ddMMyyyy", Locale.US)
            return resFormat.format(cal.time)
        }



    }


}