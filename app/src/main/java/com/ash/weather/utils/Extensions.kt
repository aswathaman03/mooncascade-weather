package com.ash.weather.utils

import android.os.Build
import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import com.ash.weather.R

fun CardView.changeBackground(isNight: Boolean) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.setCardBackgroundColor(
            if (isNight) {
                resources.getColor(
                    R.color.blueGrey,
                    null
                )
            } else {
                resources.getColor(
                    R.color.white, null
                )
            }
        )
    } else {
        this.setCardBackgroundColor(
            if (isNight) {
                ContextCompat.getColor(context!!, R.color.blueGrey)
            } else {
                ContextCompat.getColor(context!!, R.color.white)
            }
        )
    }
}

fun TextView.changeTextColor(isNight: Boolean) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.setTextColor(
            if (isNight) {
                resources.getColor(
                    R.color.white,
                    null
                )
            } else {
                resources.getColor(
                    R.color.black, null
                )
            }
        )
    } else {
        this.setTextColor(
            if (isNight) {
                ContextCompat.getColor(context!!, R.color.white)
            } else {
                ContextCompat.getColor(context!!, R.color.black)
            }
        )
    }
}

fun View.changeBackground(isNight: Boolean) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        this.setBackgroundColor(
            if (isNight) {
                resources.getColor(
                    R.color.white,
                    null
                )
            } else {
                resources.getColor(
                    R.color.black, null
                )
            }
        )
    } else {
        this.setBackgroundColor(
            if (isNight) {
                ContextCompat.getColor(context!!, R.color.white)
            } else {
                ContextCompat.getColor(context!!, R.color.black)
            }
        )
    }
}