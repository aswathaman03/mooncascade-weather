package com.ash.weather.utils

class NumberConverter {

    companion object {
        private val units = arrayOf(
            "", "One", "Two", "Three", "Four",
            "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
            "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
            "Eighteen", "Nineteen"
        )

        private val tens =
            arrayOf("", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety")


        fun convertNumberToWords(number: Int): String {
            if (number in -19..-1) return "Minus " + units[-number]
            if (number == 0) return "Zero"
            if (number in 1..19) return units[number]
            if (number in 20..99) return if ((number % 10) == 0) tens[number / 10] else tens[number / 10] + " " + units[number % 10]
            return "invalid"
        }
    }


}