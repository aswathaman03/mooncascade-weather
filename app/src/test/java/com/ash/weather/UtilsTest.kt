package com.ash.weather

import com.ash.weather.utils.NumberConverter
import org.junit.Assert.*
import org.junit.Test
import java.text.SimpleDateFormat
import java.util.*

class UtilsTest {

    @Test
    fun testDate() {
        val calendar = Calendar.getInstance()
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val outputFormat = SimpleDateFormat("dd-MMM", Locale.US)
        val inputDate = inputFormat.format(calendar.time)
        assertEquals("2019-08-29", inputDate)
        assertEquals("29-Aug", outputFormat.format(calendar.time))
    }

    @Test
    fun testDateForDb() {
        val calendar = Calendar.getInstance()
        val outputFormat = SimpleDateFormat("ddMM", Locale.US)
        assertEquals("2908", outputFormat.format(calendar.time))
    }

    @Test
    fun testZeroNumberConverter() {
        assertEquals("Zero", NumberConverter.convertNumberToWords(0))
    }

    @Test
    fun testNonZeroPositiveNumberConverter() {
        assertEquals("Twenty Five", NumberConverter.convertNumberToWords(25))
    }

    @Test
    fun testNonZeroNegativeNumberConverter() {
        assertEquals("Minus Nineteen", NumberConverter.convertNumberToWords(-19))
    }

    @Test
    fun testOutOfConverterBoundPositiveNumberConverter() {
        assertEquals("invalid", NumberConverter.convertNumberToWords(123))
    }

    @Test
    fun testOutOfConverterBoundNegativeNumberConverter() {
        assertEquals("invalid", NumberConverter.convertNumberToWords(-123))
    }
}